ARTICLE_OPTIONS = \
	--template=adass-article-template.tex \
	-f markdown+footnotes+tex_math_double_backslash \
	-t latex

PRESENTATION_OPTIONS = \
	--template=adass-presentation-template.tex \
	--filter columnfilter.py \
	--dpi=300 \
	-f markdown+tex_math_double_backslash \
	-t beamer \
	--pdf-engine=xelatex \
	--slide-level=2

PRESENTATION_IMAGES = \
	Tomasi-small.jpg \
	Giordano-small.jpg \
	python-logo-inkscape.pdf \
	python-julia-array-speed-1.pdf \
	python-julia-array-speed-2.pdf \
	python-julia-array-speed-3.pdf \
	python-julia-array-speed-4.pdf \
	julia-environment.pdf \
	two-languages.pdf \
	from-c++-to-julia.pdf

# P = paper_id,   your proceedings contribution code
# V = version,    your version # (1,2,..) since you can only upload unique files

                    # put your paper_id here (with dash, not dot)
                    # or find your template on http://www.astro.umd.edu/~teuben/adass/papers/
P = O4-5
                    # keep incrementing this after each upload of your $(P)
V = 1

#  variables for authors (comment out the ones you don't need)
FIGS = 

#  variables for editors (don't modify, for export, not authors)
YEAR   = 2018
TMPDIR = ADASS$(YEAR)_author_template
FILES  = AdassChecks.py AdassConfig.py AdassIndex.py ADASS_template.tex \
	 asp2014.bst asp2014.sty copyrightform.pdf example.eps \
	 Index.py manual2010.pdf ManuscriptInstructions.pdf PaperCheck.py \
	 README subjectKeywords.txt TexScanner.py \
	 adass2018.bib O4-5.bib Makefile

#  probably don't change these either
TAR_FILE  = $(P)_v$(V).tar.gz
ZIP_FILE  = $(P)_v$(V).zip
FILES4TAR = $(P).tex $(P).bib $(FIGS)

.phony: all export pdf clean tar zip

all:	$(P).pdf tar presentation.pdf

export:
	rm -rf $(TMPDIR)
	mkdir $(TMPDIR)
	cp -a $(FILES) $(TMPDIR)
	echo Created on `date` by `whoami`  > $(TMPDIR)/VERSION
	-git remote -v                     >> $(TMPDIR)/VERSION
	-git branch                        >> $(TMPDIR)/VERSION
	tar cf ADASS$(YEAR).tar $(TMPDIR)

# these targets are for most common unix systems, but YMMV. Modify if you need
# let the editors know you have a better way for the next 2019 ADASS team
# e.g.    latex ADASS_template; dvipdfm ADASS_template

$(P).pdf:  $(P).dvi $(FIGS)
	dvipdf $(P)

$(P).bib:                                  # bootstrap if you don't have one
	touch $(P).bib

$(P).dvi:  $(P).tex $(P).bib
	latex $(P)
	bibtex $(P)
	latex $(P)
	latex $(P)

pdf:
	pdflatex $(P)
	bibtex $(P)
	pdflatex $(P)
	pdflatex $(P)

clean:
	rm -f $(P).dvi $(P).bbl $(P).pdf

tar:
	tar zcf $(TAR_FILE) $(FILES4TAR)
	@echo Submit to ftp://ftp.astro.umd.edu/incoming/adass
	@echo e.g.   ncftpput ftp.astro.umd.edu  incoming/adass $(TAR_FILE)

zip:
	zip $(ZIP_FILE) $(FILES4TAR)
	@echo Submit to ftp://ftp.astro.umd.edu/incoming/adass
	@echo e.g.   ncftpput ftp.astro.umd.edu  incoming/adass $(ZIP_FILE)

$(P).tex: article.md adass-article-template.tex
	pandoc $(ARTICLE_OPTIONS) -o $@ $<

presentation.pdf: presentation.md  adass-presentation-template.tex $(PRESENTATION_IMAGES)
	pandoc $(PRESENTATION_OPTIONS) -o $@ $<

%.pdf: %.svg
	inkscape --export-pdf=$@ $<
