---
title: 'Towards new solutions for scientific computing: the case of Julia'
conference: ADASS 2018
author:
- Maurizio Tomasi
- Mosè Giordano
date: Nov 15 2018
aspectratio: 169
fontenc: T1
fontsize: 18
fontfamilyoptions: scaled
colorlinks: 1
colortheme: crane
mainfont: 'Noto Sans Display'
monofont: 'Noto Mono'
...



## Who are we?

[columns]

[column=0.5]

[centering]

![](Tomasi-small.jpg)

**Maurizio Tomasi (myself)**


[column=0.5]

[centering]

![](Giordano-small.jpg)

**Mosè Giordano**

[/columns]


\vspace{1em}


[columns]

[column=0.5]

- Worked on the Planck mission (calibration, simulations, data
  analysis…)
- Currently involved in other CMB experiments

[column=0.5]

- Worked on gravitational microlensing
- Author of several Julia packages
  ([github.com/giordano](https://github.com/giordano))

[/columns]


## Making Python more performant

[center]

![](python-logo-inkscape.pdf){ width=30% }

[/center]

Python is a fantastic language: easy and with a very rich
library. (And AstroPy is awesome!)

However, its speed is not impressive at all!

```
In [1]: %time x = [i*i for i in range(100_000_000)]
CPU times: user 5.27 s, sys: 860 ms, total: 6.13 s
Wall time: 6.18 s
```

<!-- C would have been 20 times faster -->

Several solutions have been developed: NumPy, [PyPy](https://www.pypy.org/),
[Numba](https://numba.pydata.org/), [Cython](https://cython.org/)…
They can be extremely performant *in their own domains*, but picking
the right one requires careful consideration.

## The two-language problem

In order to make Python codes more performant, it is common to
link them to C/C++/Fortran, using tools like `f2py`,
SWIG, Cython, and so on:

[center]

![](two-languages.pdf){ width=55% }

[/center]

These codes are complex to implement and deploy:

- Need to master many languages
- Try to write a *portable* `setup.py` for projects using `f2py`!


## Meet Julia

[center]

![](julialogo.pdf){ width=35% }

[/center]

- Relatively new language (first official release was 0.2, in Nov 2013)
- Julia 1.0 released on August, 9th 2018
- Released under the MIT license
- [julialang.org](https://julialang.org)

---

![](julia-environment.pdf){ height=80% }

---

## A taste of Julia

I'm going to play a screencast now. You can watch it again at the
following URL:
[asciinema.org/a/211906](https://asciinema.org/a/211906).

The following slides are left as a reference if you don't have an
internet connection. They are not the same as the screencast, as they
show some more features.

---

## A taste of Julia (1/5)

```julia
# One-liner definition of a function
f(x) = 3x + 1

# Floating-point
@time f(0.1)  # 0.005716 seconds (15.63 k allocations: 872.499 KiB)
@time f(0.3)  # 0.000002 seconds (5 allocations: 176 bytes)

# Integer
@time f(2)    # 0.002888 seconds (2.00 k allocations: 117.656 KiB)
@time f(10)   # 0.000001 seconds (4 allocations: 160 bytes)

# Rational
@time f(3//2) # 0.070596 seconds (209.83 k allocations: 10.785 MiB)
@time f(4//9) # 0.000005 seconds (6 allocations: 224 bytes)
```

## A taste of Julia (2/5)

```julia
# Load default packages
using Printf
using Pkg

# Install a few new packages from Internet
for name in ["Cosmology", "Measurements", "Zygote#master", "PyCall"]
    Pkg.add(name)
end

using Cosmology
c = cosmology(h=0.69, Neff=3.04, OmegaM=0.29, Tcmb=2.7255)
z = 0.1

@printf("Universe age at z=%.1f: %.1f Gyr\n", z, age_gyr(c, z))
# Prints "Universe age at z=0.1: 12.5 Gyr"
```

## A taste of Julia (3/5)

```julia
using Measurements   # Define the ± binary operator

z = 0.1 ± 0.01
println(z)
# Prints "0.1 ± 0.01"

age = age_gyr(c, z)
println(age)
# Prints "12.465336269441773 ± 0.12305608850870296"

@printf("%.2f ± %.2f Gyr\n", age.val, age.err)
# prints "12.47 ± 0.12 Gyr"
```


## A taste of Julia (4/5)

```julia
# See https://arxiv.org/abs/1810.07951
using Zygote       # Long time to compile…

g(x) = 2x + 1
println(g(1))      # Print 3
println(g'(1))     # Print 2 (derivative of g at x=1)
@code_llvm g'(1)   # Surprise! "ret i64 2"
```

## A taste of Julia (5/5)

```julia
using PyCall

@pyimport numpy.random as nr
x = nr.randn(5)
```


## JuliaAstro on GitHub

| Package | Description |
|---------|-------------|
| [`AstroImages.jl`](https://github.com/JuliaAstro/AstroImages.jl) | Visualization of astronomical images (by MG) |
| [`AstroLib.jl`](https://github.com/JuliaAstro/AstroLib.jl) | Astronomical and astrophysical routines (by MG) |
| [`AstroTime.jl`](https://github.com/JuliaAstro/AstroTime.jl) | Astronomical time keeping |
| [`Cosmology.jl`](https://github.com/JuliaAstro/Cosmology.jl) | Library of cosmological functions |
| [`DustExtinction.jl`](https://github.com/JuliaAstro/DustExtinction.jl) | Models for the interstellar extinction due to dust |
| [`ERFA.jl`](https://github.com/JuliaAstro/ERFA.jl) | Wrapper to `liberfa` |
| [`EarthOrientation.jl`](https://github.com/JuliaAstro/EarthOrientation.jl) | Earth orientation parameters from IERS tables |
| [`FITSIO.jl`](https://github.com/JuliaAstro/FITSIO.jl) | Flexible Image Transport System (FITS) file support |
| [`LombScargle.jl`](https://github.com/JuliaAstro/LombScargle.jl) | Compute Lomb-Scargle periodogram (by MG) |
| [`SPICE.jl`](https://github.com/JuliaAstro/SPICE.jl) | Julia wrapper for NASA NAIF's SPICE toolkit |
| [`SkyCoords.jl`](https://github.com/JuliaAstro/SkyCoords.jl) | Support for astronomical coordinate systems |
| [`UnitfulAstro.jl`](https://github.com/JuliaAstro/UnitfulAstro.jl) | An extension of `Unitful.jl` for astronomers |
| [`WCS.jl`](https://github.com/JuliaAstro/WCS.jl) | Astronomical World Coordinate Systems library |


## Simulating cosmological experiments with Julia

[center]

![](from-c++-to-julia.pdf){ width=85% }

[/center]


## Data science with Julia

[columns]

[column=0.5]

**The good**

[column=0.5]

**The bad**

[/columns]


[columns]

[column=0.5]

- Very fast execution for codes with lots of calculations
- Powerful features (many [numerical
  types](https://docs.julialang.org/en/v1/manual/complex-and-rational-numbers/),
  [metaprogramming](https://docs.julialang.org/en/v1/manual/metaprogramming/),
  [missing values](https://docs.julialang.org/en/v1/manual/missing/)…)
- Ability to call [C,
  Fortran](https://docs.julialang.org/en/v1/manual/calling-c-and-fortran-code/),
  [Python](https://github.com/JuliaPy/PyCall.jl),
  [R](https://github.com/JuliaInterop/RCall.jl)
- [Package management](https://docs.julialang.org/en/v1/stdlib/Pkg/)
  is rock solid (reproducible builds, like Rust's `cargo`)
- Native support for [parallel
  computing](https://docs.julialang.org/en/v1/manual/parallel-computing/)
  (no GIL here!)
- [Profiling tools](https://docs.julialang.org/en/v1/manual/profile/)
  immediately available (e.g., `--track-allocation`)

[column=0.5]

- Slow execution if every function is called just once
- Not as many packages as other languages (Python, C++, …)
- Plotting is promising
  ([PyPlot.jl](https://github.com/JuliaPy/PyPlot.jl), [Plots.jl](https://github.com/JuliaPlots/Plots.jl), [UnicodePlots.jl](https://github.com/Evizero/UnicodePlots.jl), [Makie.jl](https://github.com/JuliaPlots/Makie.jl), …), but still
  lacking
- Avoid global variables as the plague! They make the compiler highly
  inefficient

[/columns]


## When to use Julia

Julia is interesting if:

- You are going to implement a code that will do lots of calculations
  and is going to spend much time in doing it, and you will write this
  code *from scratch*.
  
. . .

- You have an existing large, monolithic code and want to turn it into
  something to be used interactively, without sacrificing speed.

. . .

- You plan to use Julia's homoiconicity to do something really
  innovative, like [Zygote.jl](https://arxiv.org/abs/1810.07951)!


## More information

- Julia compiler: [julialang.org](http://julialang.org/)
- Julia user's manual: [docs.julialang.org/en/v1](https://docs.julialang.org/en/v1/)
- Package list available at [juliaobserver.com](https://juliaobserver.com/)
- User's and developers' forums: [discourse.julialang.org](https://discourse.julialang.org/)
- Very good blogpost about Numba, Cython, and Julia: [www.stochasticlifestyle.com/why-numba-and-cython-are-not-substitutes-for-julia](http://www.stochasticlifestyle.com/why-numba-and-cython-are-not-substitutes-for-julia/)
- JuliaAstro: [github.com/JuliaAstro](https://github.com/JuliaAstro)

\vspace{1.5em}

- These slides and additional material: [bitbucket.org/Maurizio_Tomasi/adass2018-julia](https://bitbucket.org/Maurizio_Tomasi/adass2018-julia) (also available on [slideshare](https://www.slideshare.net/MaurizioTomasi/towards-new-solutions-for-scientific-computing-the-case-of-julia))
- For questions, feel free to ask me or write me an email: [maurizio.tomasi@unimi.it](mailto:maurizio.tomasi@unimi.it)



# Backup slides

## Calculations with NumPy arrays

Consider this code, where all the parameters for `f` are NumPy arrays:
```python
def f(r, x1, x2, x3, x4):
    r = x1 - x2 + x3 - x4
```

This code is executed by NumPy as if it were
```python
tmp = x1 - x2
tmp += x3
r = tmp - x4
```
thus three `for` loops are ran.

## Performance of NumPy codes

[center]

![](python-julia-array-speed-1.pdf){ width=60% }

[/center]

Source codes available at
[github.com/ziotom78/python-julia-c-](https://github.com/ziotom78/python-julia-c-).

## Calculations with C++ vectors

In C++, the code needs to be written in this way:
```c++
for(size_t i = 0; i < r.size(); ++i) {
    r[i] = x1[i] - x2[i] + x3[i] - x4[i];
}
```

We need to write the `for` loop explicitly, but there is only **one*
of them.

## Performance of NumPy/C++ codes

[center]

![](python-julia-array-speed-2.pdf){ width=60% }

[/center]

Source codes available at
[github.com/ziotom78/python-julia-c-](https://github.com/ziotom78/python-julia-c-).

## Performance of Julia programs

```julia
# 4 terms
f(r, x1, x2, x3, x4) = @. r = x1 - x2 + x3 - x4
g(r, x1, x2, x3, x4) = @. r = x1 - x2 + x3
h(r, x1, x2, x3, x4) = @. r = x1 - x2

# Etc.
```

The `@.` macro fuses all the operations on loops. Thus, `f` above is
equivalent to
```julia
function f(r, x1, x2, x3, x4)
    for i in eachindex(r)
        r[i] = x1[i] - x2[i] + x3[i] - x4[i]
    end
end
```
## Performance of NumPy, C++, and Julia codes

[center]

![](python-julia-array-speed-3.pdf){ width=60% }

[/center]

Source codes available at
[github.com/ziotom78/python-julia-c-](https://github.com/ziotom78/python-julia-c-).

## Using SIMD instructions in Julia

C++ was given an unfair advantage, as it was allowed to use SIMD
instructions (`-msse3`). Moreover, it did not check array boundaries
(Julia does automatically).

In Julia, we can use the `@inbounds` and `@simd` macro
to make Julia code equivalent to C++:

```julia
function f(r, x1, x2, x3, x4)
    @inbounds @simd for i in eachindex(r)
        r[i] = x1[i] - x2[i] + x3[i] - x4[i]
    end
end
```

## Performance of NumPy, C++, and Julia codes

[center]

![](python-julia-array-speed-3.pdf){ width=60% }

[/center]

Source codes available at
[github.com/ziotom78/python-julia-c-](https://github.com/ziotom78/python-julia-c-).

## Performance of NumPy, C++, and Julia codes

[center]

![](python-julia-array-speed-4.pdf){ width=60% }

[/center]

Source codes available at
[github.com/ziotom78/python-julia-c-](https://github.com/ziotom78/python-julia-c-).
