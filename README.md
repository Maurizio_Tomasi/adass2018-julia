# Towards new solutions for scientific computing: the case of Julia

This repository contains the source code for the presentation and the
paper titled *Towards new solutions for scientific computing: the case
of Julia*, to be presented at ADASS2018 (College Park, Maryland, US).

Both the presentation and the paper have been written in Markdown and
are converted to LaTeX and PDF using [pandoc](https://pandoc.org/).

Run `make` to produce both PDFs.
